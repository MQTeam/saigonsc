'use strict';

angular.module('tqlsampleuiApp')
    .controller('TrafficCtrl', function ($scope, $location, $timeout, ApiService) {

        var vm = this;
        var series = null;

        vm.printDate = function (value) {
            return moment(value, "YYYYMMDDHHmmss").format('MMMM Do YYYY, h:mm:ss a')
        }

        vm.getAllData = function () {
            ApiService.getAllTrafficCall(function (rs) {
                rs = JSON.parse(rs).Find.Result;
                console.log(rs);
                $timeout(function () {
                    vm.data = rs;
                })

            })
        }

        vm.startWs = function () {
            ApiService.startSubscriptionTraffic(function (rs) {
                console.log(rs);
                var path = "Saigon.Mobility.TrafficInfo.streets";
                var key = Object.keys(rs)[0];
                var obj = {
                    name: rs[key][path + ".streetName"]._Value,
                    bike: rs[key][path + ".bikeCount"]._Value,
                    car: rs[key][path + ".carCount"]._Value,
                    truck: rs[key][path + ".truckCount"]._Value,
                    time: new Date().getTime(),
                }
                console.log(obj);
                series[0].addPoint([parseFloat(obj.time), parseInt(obj.bike)], true, true);
                series[1].addPoint([parseFloat(obj.time), parseInt(obj.car)], true, true);
                series[2].addPoint([parseFloat(obj.time), parseInt(obj.truck)], true, true);
            })
        }

        vm.stopWs = function () {
            ApiService.stopSubscriptionTraffic();
        }

        function loadGraph() {
            Highcharts.chart('container', {
                chart: {
                    type: 'spline',
                    animation: Highcharts.svg, // don't animate in old IE
                    marginRight: 10,
                    events: {
                        load: function () {

                            // set up the updating of the chart each second
                            series = this.series;
                            // setInterval(function () {
                            //     var x = (new Date()).getTime(), // current time
                            //         y = Math.random();
                            //     series.addPoint([x, y], true, true);
                            // }, 1000);

                        }
                    }
                },
                title: {
                    text: 'Live Traffic data'
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                            Highcharts.numberFormat(this.y, 2);
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    name: 'bike',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                }, {
                    name: 'car',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                }, {
                    name: 'truck',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                }]
            });
        }

        $timeout(function () {
            loadGraph();
            vm.getAllData();
        });

    });
