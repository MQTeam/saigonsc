'use strict';

angular.module('tqlsampleuiApp')
    .controller('SmartCityCtrl', function ($scope, $location, $timeout, ApiService) {

        var vm = this;

        vm.cityList = SMART_CITY.city_list;

        vm.startLight = function () {
            return ApiService.startLight().then(function (rs) {
                console.log(rs);
            });
        }
        vm.endLight = function () {
            return ApiService.endLight().then(function (rs) {
                console.log(rs);
            });
        }
        vm.offLight = function () {
            return ApiService.offLight().then(function (rs) {
                console.log(rs);
            });
        }
        vm.startMotor = function () {
            return ApiService.startMotor().then(function (rs) {
                console.log(rs);
            });
        }
        vm.endMotor = function () {
            return ApiService.endMotor().then(function (rs) {
                console.log(rs);
            });
        }

        vm.changeCity = function () {
            return ApiService.changeCity(vm.selectedCity).then(function (rs) {
                console.log(rs);
            });
        }

        vm.getCityData = function () {
            return ApiService.getCityData().then(function (rs1) {
                console.log(rs1);
                vm.cityData = rs1["0"].Find.Result.ExternalEnv.EnvDetails;
            });
        }
        vm.checkingLightColor = SMART_CITY.checkingLightColor;

        vm.endCityData = function () {
            return ApiService.endCityData(vm.selectedCity).then(function (rs) {
                console.log(rs);
            });
        }

        vm.offCityData = function () {
            return ApiService.offCityData(vm.selectedCity).then(function (rs) {
                console.log(rs);
            });
        }

        vm.startSensor = function () {
            return ApiService.startSensor().then(function (rs) {
                console.log(rs);
            });
        }

        vm.endSensor = function () {
            return ApiService.endSensor().then(function (rs) {
                console.log(rs);
            });
        }
        vm.getAmbiance = function () {
            return ApiService.getAmbiance().then(function (rs) {
                console.log(rs);
                vm.ambiance = rs["0"].Find.Result.AmbianceSensorModel.AmbianceValue._Known;
            });
        }
        vm.getHumidity = function () {
            return ApiService.getHumidity().then(function (rs) {
                console.log(rs);
                vm.humidity = rs["0"].Find.Result.HumiditySensorModel.HumidityValue._Known;
            });
        }
        vm.getTemperature = function () {
            return ApiService.getTemperature().then(function (rs) {
                console.log(rs);
                vm.temperature = rs["0"].Find.Result.TempSensorModel.TempValueInC._Known;
            });
        }

        vm.startWS = function () {
            return ApiService.startSubscriptionAmbiance(function (data) {
                console.log(data);
                var key = Object.keys(data)[0];
                console.log(data[key]["Atomiton.Sensor.AmbianceSensorModel.AmbianceValue"]);
                var value = data[key]["Atomiton.Sensor.AmbianceSensorModel.AmbianceValue"];
                if(value._Known){
                    var x = parseInt(value._Timestamp), // current time
                        y = parseFloat(value._Known);
                    console.log(x, y);
                    series.addPoint([x, y], true, true);
                }

            });
        }

        vm.stopWS = function () {
            return ApiService.stopSubscriptionAmbiance();
        }

        $scope.$watch(function () {
            return vm.selectedCity;
        }, function (newV, oldV) {
            if (typeof newV !== 'undefined' && newV !== oldV) {
                vm.changeCity();
            }
        });

        var series = null;

        function loadGraph() {
            Highcharts.chart('container', {
                chart: {
                    type: 'spline',
                    animation: Highcharts.svg, // don't animate in old IE
                    marginRight: 10,
                    events: {
                        load: function () {

                            // set up the updating of the chart each second
                            series = this.series[0];
                            // setInterval(function () {
                            //     var x = (new Date()).getTime(), // current time
                            //         y = Math.random();
                            //     series.addPoint([x, y], true, true);
                            // }, 1000);

                        }
                    }
                },
                title: {
                    text: 'Live Ambiance data'
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                            Highcharts.numberFormat(this.y, 2);
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    name: 'Ambiance',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                }]
            });
        }

        $timeout(function () {
            loadGraph();
            // startWS();
        });
    });
