'use strict';

angular.module('tqlsampleuiApp')
    .controller('AgriCtrl', function ($scope, $location, $timeout, ApiService) {

        var vm = this;
        var series = null;

        vm.printDate = function (value) {
            return moment(value, "YYYYMMDDHHmmss").format('MMMM Do YYYY, h:mm:ss a')
        }

        vm.getAllData = function () {
            ApiService.getAllAgriCall(function (rs) {
                rs = JSON.parse(rs).Find.Result;
                $timeout(function () {
                    vm.data = rs;
                })
            })
        }

        vm.startWs = function () {
            ApiService.startSubscriptionAgri(function (rs) {
                console.log(rs);
                var path = "Saigon.Agriculture.FieldFarm.zones";
                var key = Object.keys(rs)[0];
                var obj = {
                    name: rs[key][path + ".zoneName"][0]._Value,
                    tempC: rs[key][path + ".tempC"][0]._Value,
                    ambiance: rs[key][path + ".ambiance"][0]._Value,
                    humidity: rs[key][path + ".humidity"][0]._Value,
                    time: new Date().getTime(),
                }
                console.log(obj);
                series[0].addPoint([parseFloat(obj.time), parseInt(obj.ambiance)], true, true);
                series[1].addPoint([parseFloat(obj.time), parseInt(obj.tempC)], true, true);
                series[2].addPoint([parseFloat(obj.time), parseInt(obj.humidity)], true, true);
            })
        }

        vm.stopWs = function () {
            ApiService.stopSubscriptionAgri();
        }

        function loadGraph() {
            Highcharts.chart('container', {
                chart: {
                    type: 'spline',
                    animation: Highcharts.svg, // don't animate in old IE
                    marginRight: 10,
                    events: {
                        load: function () {

                            // set up the updating of the chart each second
                            series = this.series;
                            // setInterval(function () {
                            //     var x = (new Date()).getTime(), // current time
                            //         y = Math.random();
                            //     series.addPoint([x, y], true, true);
                            // }, 1000);

                        }
                    }
                },
                title: {
                    text: 'Live farmzone data'
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        text: 'Value'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function () {
                        return '<b>' + this.series.name + '</b><br/>' +
                            Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                            Highcharts.numberFormat(this.y, 2);
                    }
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    name: 'tempC',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                },{
                    name: 'ambiance',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                },{
                    name: 'humidity',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: 0
                            });
                        }
                        return data;
                    }())
                }]
            });
        }

        $timeout(function () {
            loadGraph();
            vm.getAllData();
        });
    });
