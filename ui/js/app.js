angular
    .module('tqlsampleuiApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial',
        'ngMessages',
        'ui.codemirror',
        'duScroll',
        'angular-loading-bar'
    ])
    .config(function ($routeProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;


        $routeProvider
            .when('/', {
                redirectTo: '/home'
            })
            .when('/home', {
                templateUrl: 'js/routes/home/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/smartCity', {
                templateUrl: 'js/routes/smartCity/smartCity.html',
                controller: 'SmartCityCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/shrimpFarm', {
                templateUrl: 'js/routes/shrimpFarm/shrimpFarm.html',
                controller: 'ShrimpFarmCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/traffic', {
                templateUrl: 'js/routes/traffic/traffic.html',
                controller: 'TrafficCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/camera', {
                templateUrl: 'js/routes/camera/camera.html',
                controller: 'CameraCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .when('/agri', {
                templateUrl: 'js/routes/agri/agri.html',
                controller: 'AgriCtrl',
                controllerAs: 'vm',
                resolve: {}
            })
            .otherwise({
                redirectTo: '/'
            });
    });
