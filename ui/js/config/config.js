var SMART_CITY = {
    URL: "http://118.69.32.39:8080/fid-MFIBIN6QAAAH6AABAE2AV2W3",
    WS_URL: "ws://118.69.32.39:8080/fid-MFIBIN6QAAAH6AABAE2AV2W3WS",
    city_list: ["Hanoi, VN", "Ottawa, CA", "Bangalore, IN", "Colorado, US"],
    checkingLightColor: function (value) {
        value = parseInt(value);
        if (value < 10) {
            return "white";
        }
        return "purple";
    },
    QUERIES: {
        START_LIGHT: '<ScheduleTQLCode><ScheduleInterval>1sec</ScheduleInterval><ScheduleName>RGBLightSchedule</ScheduleName><ActionCode><executeQuery><QueryString><Query><Find format="version,known"><RGBModel><rgbId ne="" /></RGBModel></Find><if condition="$Response.Message.Value/Find/Status eq \'Success\'"><then><JavaScript>var r = Math.floor(Math.random() * (3 - 1) + 1); var r1 = Math.floor(Math.random() * (8 - 0) + 0); var RGBStr = "RGB-2" + ":" + r1.toString(2); sffContext.setContextData("RGB", RGBStr);</JavaScript><SetResponseData><key>Message.Value.Find.Result.RGBModel.RGBString.Value</key><value>[:$ContextData.RGB:]</value></SetResponseData><Update><from>Result</from><Include>$Response.Message.Value.Find</Include></Update></then><else><DelResponseData key="Message.Value.Find" /><SetResponseData key="Message.Value.status" value="Failed" /><SetResponseData key="Message.Value.Message" value="RGB is not initialized" /></else></if></Query></QueryString></executeQuery></ActionCode></ScheduleTQLCode>',
        END_LIGHT: '<RemoveScheduledTQLCode><ScheduleName>RGBLightSchedule</ScheduleName></RemoveScheduledTQLCode>',
        OFF_LIGHT: '<Query><Find format="version,known"><RGBModel><rgbId ne="" /></RGBModel></Find><if condition="$Response.Message.Value/Find/Status eq \'Success\'"><then><SetResponseData><key>Message.Value.Find.Result.RGBModel.RGBString.Value</key><value>RGB-2:000</value></SetResponseData><Update><from>Result</from><Include>$Response.Message.Value.Find</Include></Update></then><else><DelResponseData key="Message.Value.Find" /><SetResponseData key="Message.Value.status" value="Failed" /><SetResponseData key="Message.Value.Message" value="RGB is not initialized" /></else></if></Query>',
        START_MOTOR: '<ScheduleTQLCode><ScheduleInterval>2sec</ScheduleInterval><ScheduleName>MotorScheduleOne</ScheduleName><ActionCode><executeQuery><QueryString><Query><Find format="version,known"><ServoModel><servoId ne="" /></ServoModel></Find><if condition="$Response.Message.Value/Find/Status eq \'Success\'"><then><JavaScript>var rr = Math.floor([:$Response.Message.Value.Find.Result.ServoModel.Angle.Value:] + 45); \n' +
        '                if ( rr &lt; 180) \n' +
        '                {\n' +
        '                sffContext.setContextData("Angle", rr.toString());\n' +
        '                } else {\n' +
        '                sffContext.setContextData("Angle", "0");\n' +
        '                }</JavaScript><SetResponseData><key>Message.Value.Find.Result.ServoModel.Angle.Value</key><value>[:$ContextData.Angle:]</value></SetResponseData><Update><from>Result</from><Include>$Response.Message.Value.Find</Include></Update></then><else><DelResponseData key="Message.Value.Find" /><SetResponseData key="Message.Value.status" value="Failed" /><SetResponseData key="Message.Value.Message" value="Servo is not initialized" /></else></if></Query></QueryString></executeQuery></ActionCode></ScheduleTQLCode>',
        END_MOTOR: '<RemoveScheduledTQLCode><ScheduleName>MotorScheduleOne</ScheduleName></RemoveScheduledTQLCode>',
        CHANGE_CITY: '<Query><DeleteAll><ExternalEnv><DCBaseURL ne="" /></ExternalEnv></DeleteAll><Create><ExternalEnv><DCBaseURL>http://api.openweathermap.org/data/2.5/weather</DCBaseURL><DCAPIKey>59c00050441e2b41979a1fcf9714ae52</DCAPIKey><OutputFormat>xml</OutputFormat><DCQueryParam>{0}</DCQueryParam><EnvDetails><Temperature>0</Temperature><Humidity>0</Humidity><Light>0</Light><Pressure>0</Pressure><LastUpdatedByProvider>0</LastUpdatedByProvider></EnvDetails><EmulateSensing>true</EmulateSensing><SensingInterval>60</SensingInterval></ExternalEnv></Create></Query>',
        GET_CITY_DATA: '<Query><Find format="known,version"><ExternalEnv><DCBaseURL ne="" /></ExternalEnv></Find></Query>',
        END_CITY: '<Query><DeleteAll><ExternalEnv><DCBaseURL ne="" /></ExternalEnv></DeleteAll><Create><ExternalEnv><DCBaseURL>http://api.openweathermap.org/data/2.5/weather</DCBaseURL><DCAPIKey>59c00050441e2b41979a1fcf9714ae52</DCAPIKey><OutputFormat>xml</OutputFormat><DCQueryParam>Ottawa,CA</DCQueryParam><EnvDetails><Temperature>0</Temperature><Humidity>0</Humidity><Light>0</Light><Pressure>0</Pressure><LastUpdatedByProvider>0</LastUpdatedByProvider></EnvDetails><EmulateSensing>false</EmulateSensing><SensingInterval>60</SensingInterval></ExternalEnv></Create></Query>',
        OFF_CITY: '<Query><Find format="version,known"><RGBModel><rgbId ne="" /></RGBModel></Find><if condition="$Response.Message.Value/Find/Status eq \'Success\'"><then><SetResponseData><key>Message.Value.Find.Result.RGBModel.RGBString.Value</key><value>RGB-3:000</value></SetResponseData><Update><from>Result</from><Include>$Response.Message.Value.Find</Include></Update></then><else><DelResponseData key="Message.Value.Find" /><SetResponseData key="Message.Value.status" value="Failed" /><SetResponseData key="Message.Value.Message" value="RGB is not initialized" /></else></if></Query>',
        START_SENSOR: '<Query><Find format="version,Known"><SerialPortModel><ID ne="" /></SerialPortModel></Find><if condition="$Response.Message.Value/Find/Status eq \'Success\'"><then><SetContextData key="PortName"><Value>[:$Response.Message.Value.Find.Result.SerialPortModel.PortName.Known:]</Value></SetContextData><SetContextData key="Baudrate"><Value>[:$Response.Message.Value.Find.Result.SerialPortModel.Baudrate:]</Value></SetContextData><executeQuery><QueryString><Query><DeleteAll format="version,current"><MCUSensorModel><sensorId ne="" /></MCUSensorModel></DeleteAll><DeleteAll format="version,current"><RGBModel><rgbId ne="" /></RGBModel></DeleteAll><DeleteAll format="version,current"><ServoModel><servoId ne="" /></ServoModel></DeleteAll><DeleteAll><USBCameraModel><camId ne="" /></USBCameraModel></DeleteAll><Create><MCUSensorModel><PerifParams><Peripheral>serial</Peripheral><InterfacePort>[:$ContextData.PortName:]</InterfacePort><Baudrate>[:$ContextData.Baudrate:]</Baudrate><Format>ascii</Format><Operation>receive</Operation><Payload>$Null()</Payload></PerifParams><SensorData>$Null()</SensorData><InitSubscribers>true</InitSubscribers></MCUSensorModel></Create><Create><RGBModel><Peripheral>serial</Peripheral><InterfacePort>[:$ContextData.PortName:]</InterfacePort><Baudrate>[:$ContextData.Baudrate:]</Baudrate><format>ascii</format><Operation>transmit</Operation><RGBString>RGB-1:000</RGBString></RGBModel></Create><Create><ServoModel><Peripheral>serial</Peripheral><InterfacePort>[:$ContextData.PortName:]</InterfacePort><Baudrate>[:$ContextData.Baudrate:]</Baudrate><format>ascii</format><Operation>transmit</Operation><Angle>45</Angle><received>$Null()</received></ServoModel></Create><Create><USBCameraModel><Image>$Null()</Image><Mode>Camera</Mode><ImageName>ucam_mypic.jpeg</ImageName><Height>480</Height><Width>640</Width><SaveToDisk>true</SaveToDisk></USBCameraModel></Create></Query></QueryString></executeQuery><SetResponseData key="Message.Value.Status" value="Success" /><SetResponseData key="Message.Value.Message" value="IoTKit Things Instantiated Successfully" /></then><else><SetResponseData key="Message.Value.Status" value="WARN" /><SetResponseData key="Message.Value.Message" value="Serial Port not detected" /></else></if></Query>',
        END_SENSOR: '<Query><Find format="version"><MCUSensorModel><sensorId ne="" /></MCUSensorModel></Find><SetResponseData><key>Message.Value.Find.Result.MCUSensorModel.InitSubscribers.Value</key><value>false</value></SetResponseData><SetResponseData><key>Message.Value.Find.Result.MCUSensorModel.SensorData.Value</key><value>$Null()</value></SetResponseData><Update><from>Result</from><Include>$Response.Message.Value.Find</Include></Update></Query>',
        GET_AMBIANCE: '<Query><Find format="version,known"><AmbianceSensorModel><AmbSensorId ne="" /></AmbianceSensorModel></Find></Query>',
        GET_HUMIDITY: '<Query><Find format="version,known"><HumiditySensorModel><HumSensorId ne="" /></HumiditySensorModel></Find></Query>',
        GET_TEMPERATURE: '<Query><Find format="version,known"><TempSensorModel><TempSensorId ne="" /></TempSensorModel></Find></Query>',
        SUBSCRIPTION: '<Query Storage="TqlSubscription"><Save><TqlSubscription Label="AmbianceSensorModel" sid="20"><Topic>*Atomiton.Sensor.AmbianceSensorModel*</Topic></TqlSubscription></Save></Query>'

    }
}

var TRAFFIC = {
    URL: 'ws://18.221.208.39:8080/fid-trafficws',
    QUERIES: {
        GET_ALL: '<Query><Find  limit="20"><TrafficInfo><trafficId ne="" /></TrafficInfo></Find></Query>',
        SUBSCRIPTION: '<Query Storage="TqlSubscription"><Save><TqlSubscription Label="trafficdata" sid="20"><Topic>*Saigon.Mobility.TrafficInfo*</Topic></TqlSubscription></Save></Query>'
    }
};

var AGRI = {
    URL: 'ws://18.221.208.39:8080/fid-agriculturews',
    QUERIES: {
        GET_ALL: '<Query><Find><FieldFarm><fieldFarmId ne="" /></FieldFarm></Find></Query>',
        SUBSCRIPTION: '<Query Storage="TqlSubscription"><Save><TqlSubscription Label="farmingdata" sid="20"><Topic>*Saigon.Agriculture.FieldFarm*</Topic></TqlSubscription></Save></Query>'
    }
};


var SHRIMPFARM = {
    URL: 'ws://18.221.208.39:8080/fid-shrimpfarmws',
    QUERIES: {
        GET_ALL: '<Query><Find><ShrimpRegion><shrimpRegionId ne="" /></ShrimpRegion></Find></Query>',
        SUBSCRIPTION: '<Query Storage="TqlSubscription"><Save><TqlSubscription Label="shrimpfarmdata" sid="20"><Topic>*Saigon.ShrimpFarm.ShrimpRegion*</Topic></TqlSubscription></Save></Query>'
    }
}