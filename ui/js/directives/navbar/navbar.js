'use strict';

/**
 * @ngdoc directive
 * @name greenhouseuiApp.directive:navBar/navBar
 * @description
 * # navBar/navBar
 */
angular.module('tqlsampleuiApp')
    .directive('navBarTop', function () {
        return {
            templateUrl: "./js/directives/navbar/navbar.html",
            restrict: 'E',
            link: function postLink(scope, element, attrs) {

                scope.isState = function (name) {
                    return window.location.hash == name;
                }
            }
        };
    });
