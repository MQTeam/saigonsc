'use strict';

/**
 * @ngdoc directive
 * @name greenhouseuiApp.directive:navBar/navBar
 * @description
 * # navBar/navBar
 */
angular.module('tqlsampleuiApp')
    .directive('footer', function () {
        return {
            templateUrl: "./js/directives/footer/footer.html",
            restrict: 'E',
            link: function postLink(scope, element, attrs) {


            }
        };
    });
