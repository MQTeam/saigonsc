'use strict';

angular.module('tqlsampleuiApp')
    .service('ApiService', ['$http', '$q', 'GeneralService', '$timeout', function ($http, $q, GeneralService, $timeout) {


        var self = this;
        var SMART_CITY_QUERIES = SMART_CITY.QUERIES;
        var TRAFFIC_QUERIES = TRAFFIC.QUERIES;
        var AGRI_QUERIES = AGRI.QUERIES;
        var SHRIMPFARM_QUERIES = SHRIMPFARM.QUERIES;

        self.$timeout = $timeout;

        function simpleApiCall(req) {
            return $http(req);
        }

        function simpleApiCallWS(url, query, isStop, callback) {

            var x2js = new X2JS();

            /*open a websocket for the force sensor*/
            var fws = new WebSocket(url);
            fws.onopen = function () {
                // Web Socket is connected, send data using send()
                //console.log("ws open");
                fws.send(query);
            };

            fws.onmessage = function (e) {
                var server_message = e.data;

                if (callback) {
                    callback(server_message);
                    if (isStop)
                        fws.close();
                }
            };
            fws.onerror = function (error) {
                console.log('Error detected: ' + error);
            };
            fws.onclose = function () {
                console.log("ws close");
            };

            return fws;
        };


        function apiCall(req, path, object, type, params, includeRawData) {
            if (params)
                for (var i = 0; i < params.length; i++) {
                    req.data = GeneralService.replaceAll(req.data, '{' + i + '}', params[i]);
                }

            return $http(req).then(function (response) {

                var x2js = new X2JS();

                var json = x2js.xml_str2json(response.data);

                //check if json is null
                if (json == null) {
                    json = x2js.xml_str2json("<data>" + response.data + "</data>");
                }

                if (path) {
                    var pathArray = path.split('.');
                    for (var i = 0; i < pathArray.length; i++) {
                        json = json[pathArray[i]];
                    }
                }

                if (typeof json === 'undefined') {
                    return [];
                }

                if (!angular.isArray(json)) {
                    json = [json];
                }

                //remove Object

                if (object)
                    if (type == "SIMPLE") {
                        for (var i = 0; i < json.length; i++) {
                            if (typeof json[i] !== 'undefined') {
                                json[i] = json[i][object];
                            } else {

                            }
                        }
                    }

                if (includeRawData) {
                    json = {
                        data: json,
                        rawData: response.data
                    };
                }


                return json;
            });
        };

        function apiCallWS(url, query, callback) {

            var x2js = new X2JS();

            /*open a websocket for the force sensor*/
            var fws = new WebSocket(url);
            fws.onopen = function () {
                // Web Socket is connected, send data using send()
                //console.log("ws open");
                fws.send(query);
            };

            fws.onmessage = function (e) {
                var server_message = e.data;

                var tagArray = server_message.split(/[<>]/);

                for (var i = 0; i < tagArray.length; i++) {
                    if (tagArray[i].trim().indexOf(" ") >= 0 && tagArray[i].trim().indexOf("Equipment") === 0) {
                        var replaceS = GeneralService.replaceAll(tagArray[i], " ", "-");
                        server_message = GeneralService.replaceAll(server_message, tagArray[i], replaceS);
                    }
                }


                var jsonObj = x2js.xml_str2json(server_message);
                var result = (jsonObj && typeof jsonObj.TqlNotification !== 'undefined') ? jsonObj.TqlNotification.Update : 0;
                if (!result) {
                    result = (jsonObj && typeof jsonObj.TqlNotification !== 'undefined') ? jsonObj.TqlNotification.Create : 0;
                }
                if (callback) {
                    if (typeof result !== 'undefined' && result) {
                        var key = Object.keys(result)[0];
                        var changeKey = GeneralService.replaceAll(key, "-", " ");
                        var obj = {};
                        obj[changeKey] = result[key];
                        callback(obj);
                    }
                    // else {
                    //     callback(0);
                    // }
                }
            };
            fws.onerror = function (error) {
                console.log('Error detected: ' + error);
            };
            fws.onclose = function () {
                console.log("ws close");
            };

            return fws;
        };

        var ambianceWS = null;
        var trafficWS = null;
        var agricultureWS = null;
        var shrimpFarmWS = null;

        self.startSubscriptionAmbiance = function (callback) {
            ambianceWS = apiCallWS(SMART_CITY.WS_URL, SMART_CITY_QUERIES.SUBSCRIPTION, callback);
            return ambianceWS;
        };

        self.stopSubscriptionAmbiance = function () {
            return ambianceWS.close();
        }


        // /*========= WS ==========*/
        // self.startSubscriptionWSTankMotor = function (callback) {
        //   return apiCallWS(API.WS.TANK_MOTOR, callback);
        // };
        // self.startSubscriptionWSEnvProfile = function (callback) {
        //   return apiCallWS(API.WS.ENV_PROFILE, callback);
        // };
        // self.startSubscriptionWSPulseMeter = function (callback) {
        //   return apiCallWS(API.WS.PULSE_METER, callback);
        // };
        // self.startSubscriptionWSEnergyCumulative = function (callback) {
        //   return apiCallWS(API.WS.ENERGY_CUMULATIVE, callback);
        // };
        // self.startSubscriptionNotification = function (callback) {
        //   return apiCallWS(API.NOTIFICATION_SUBSCRIPTION.SUBSCRIPTION, callback);
        // };
        // self.startSubscriptionActivity = function (callback) {
        //   return apiCallWS(API.WS.ACTIVITY, callback);
        // };
        // self.startSubscriptionHeatTrace = function (callback) {
        //   return apiCallWS(API.WS.HEAT_TRACE, callback);
        // };

        self.startLight = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.START_LIGHT
            };
            return apiCall(req);
        };
        self.endLight = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.END_LIGHT
            };
            return apiCall(req);
        };
        self.offLight = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.OFF_LIGHT
            };
            return apiCall(req);
        };
        self.startMotor = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.START_MOTOR
            };
            return apiCall(req);
        };

        self.endMotor = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.END_MOTOR
            };
            return apiCall(req);
        };

        self.changeCity = function (name) {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.CHANGE_CITY
            };
            return apiCall(req, null, null, null, [name]);
        };

        self.getCityData = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.GET_CITY_DATA
            };
            return apiCall(req);
        };
        self.endCityData = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.END_CITY
            };
            return apiCall(req);
        };
        self.offCityData = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.OFF_CITY
            };
            return apiCall(req);
        };
        self.startSensor = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.START_SENSOR
            };
            return apiCall(req);
        };
        self.endSensor = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.END_SENSOR
            };
            return apiCall(req);
        };
        self.getAmbiance = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.GET_AMBIANCE
            };
            return apiCall(req);
        };
        self.getHumidity = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.GET_HUMIDITY
            };
            return apiCall(req);
        };
        self.getTemperature = function () {
            var req = {
                method: 'POST',
                url: SMART_CITY.URL,
                data: SMART_CITY_QUERIES.GET_TEMPERATURE
            };
            return apiCall(req);
        };

        self.getAllTrafficCall = function (callback) {
            return simpleApiCallWS(TRAFFIC.URL, TRAFFIC_QUERIES.GET_ALL, true, callback);
        }

        self.startSubscriptionTraffic = function (callback) {
            trafficWS = apiCallWS(TRAFFIC.URL, TRAFFIC_QUERIES.SUBSCRIPTION, callback);
            return trafficWS;
        };

        self.stopSubscriptionTraffic = function () {
            return trafficWS.close();
        }

        // Agriculture
        self.getAllAgriCall = function (callback) {
            return simpleApiCallWS(AGRI.URL, AGRI_QUERIES.GET_ALL, true, callback);
        }

        self.startSubscriptionAgri = function (callback) {
            agricultureWS = apiCallWS(AGRI.URL, AGRI_QUERIES.SUBSCRIPTION, callback);
            return agricultureWS;
        };

        self.stopSubscriptionAgri = function () {
            return agricultureWS.close();
        }

        // ShrimpFarm
        self.getAllShrimpFarmCall = function (callback) {
            return simpleApiCallWS(SHRIMPFARM.URL, SHRIMPFARM_QUERIES.GET_ALL, true, callback);
        }

        self.startSubscriptionShrimpFarm = function (callback) {
            shrimpFarmWS = apiCallWS(SHRIMPFARM.URL, SHRIMPFARM_QUERIES.SUBSCRIPTION, callback);
            return shrimpFarmWS;
        };

        self.stopSubscriptionShrimpFarm = function () {
            return shrimpFarmWS.close();
        }
        
        return self;
    }]);
